var DSSV = [];

let dataJson = localStorage.getItem("DSSV_LOCAL");

if (dataJson != null) {
  var dataArr = JSON.parse(dataJson);
  DSSV = dataArr.map(function (item) {
    var sv = new SinhVien(
      item.maSv,
      item.tenSv,
      item.email,
      item.pass,
      item.diemToan,
      item.diemLy,
      item.diemHoa
    );
    return sv;
  });
  renderDSSV(DSSV);
}

function themSV() {
  var sv = layThongTinTuFrom();
  DSSV.push(sv);
  console.log(DSSV);
  var dssvJson = JSON.stringify(DSSV);
  localStorage.setItem("DSSV_LOCAL", dssvJson);
  renderDSSV(DSSV);
}

function xoaSV(idSV) {
  var viTri = timKiemViTri(idSV, DSSV);
  console.log(viTri);
  if (viTri != -1) {
    DSSV.splice(viTri, 1);
    renderDSSV(DSSV);
  }
}

function suaSV(idSV) {
  var viTri = timKiemViTri(idSV, DSSV);
  if (viTri != -1) {
    var sv = DSSV[viTri];
    document.getElementById("txtMaSV").disabled = true;
  }
}
